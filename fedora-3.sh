#!/bin/bash
# Post installazione di Fedora (3)

# RPM Fusion
# Configurazione
sudo dnf install https://mirrors.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm https://mirrors.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm
sudo dnf group upgrade core

# Multimedia
sudo dnf swap ffmpeg-free ffmpeg --allowerasing
sudo dnf install --setop="install_weak_deps=False" gstreamer1-plugins-bad-freeworld gstreamer1-plugins-ugly qt5-qtwebengine-freeworld
#sudo dnf group upgrade multimedia --setop="install_weak_deps=False" --exclude=PackageKit-gstreamer-plugin
sudo dnf group upgrade sound-and-video

# Riproduzione DVD
#sudo dnf install rpmfusion-free-release-tainted
#sudo dnf install libdvdcss

# Hardware accelerated codec
sudo dnf install intel-media-driver

# Nvidia drivers (con cautela!)
#sudo dnf update
#sudo dnf install akmod-nvidia
#sudo dnf install xorg-x11-drv-nvidia-cuda

# Pacchetti software da RPM Fusion
# audio e video
sudo dnf install audacious-plugins-freeworld audacious-plugins-freeworld-aac audacious-plugins-freeworld-ffaudio audacious-plugins-freeworld-mms
sudo dnf install kdenlive vlc
# videogiochi:
#sudo dnf install lutris steam
# altro (client p2p file sharing):
#sudo dnf install amule
