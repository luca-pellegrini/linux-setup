# Checklist per configurare una nuova installazione GNU/Linux

Cose da fare dopo l'installazione di una nuova distro GNU/Linux (o reinstallazione/aggiornamento)

## Istruzioni per Linux Mint (e forse altre derivate Ubuntu)

### Prima serie di azioni

1. In **Impostazioni di sistema**: 
   1. **Accessibilità**: Testo ingrandito --> ON
   2. **Affiancamento finestre**: "Trascinando una finestra sul bordo superiore..." --> ON
   3. **Applet**: 
      * Calendario --> configura --> formato data personalizzato: ON --> `%H:%M%n%a, %d/%m/%Y`
      * Cambio area di lavoro --> aggiungi --> configura --> Tipo di schermo: bottoni semplici
   4. **Applicazioni d'avvio**: OFF su Applet coda di stampa, Blueman, Gestore Aggiornamenti, mintwelcome
   5. **Generali**: Riavvia Cinnamon --> Frequenza di controllo: 90s
   **Origini software**: Selezionare mirror locali per i repository ufficiali: es. https://ftp.fau.de/mint/packages (principale), https://mirror.ubuntu.ikoula.com (base)
2. Esegui script in `.../Data/Post-installazione-Linux/Linux-Mint-1.sh`
   1. Coonfigura e attiva il firewall ufw
   2. Aggiorna lista dei pacchetti installabili/aggiornabili (`sudo apt update`)
   3. Aggiorna pacchetti software disponibili (`sudo apt upgrade`)
   4. Crea la cartella /Data, in cui poi sarà montata la partizione ext4 "Data"
   5. Modifica file `/etc/fstab` (vedi /Data/Post-installazione-Linux/modifiche-fstab.md)
3. Fai un backup con timeshift
4. Riavvia

**Script bash n.1**: (probabilmente) `sh /media/luca/Data/Post-installazione-Linux/Linux-Mint-1.sh`

### Seconda serie di azioni

1. Esegui script in `/Data/Post-installazione-Linux/Linux-Mint-2.sh`
   1. Collega alcune cartelle presenti in /Data (Documenti, Immagini, Musica, ...) alle corrispettive cartelle in Home (crea symlinks)
   2. Personalizza menu di GRUB
   3. Installa pacchetti software aggiuntivi (da repo ufficiali, PPA per Strawberry, Calibre, yt-dlp, flatpak) e rimuove software preinsallato non desiderato
2. Imposta uno schermo personalizzato per il desktop
3. Fai un backup con timeshift
4. Installa driver Nvidia (**Gestore dei driver**)
5. Riavvia

**Script bash n.2**: `sh /Data/Post-installazione-Linux/Linux-Mint-2.sh`

## Istruzioni per Fedora

...

**Script bash n.1** (firewall e primi aggiornamenti) punti 2-4: 
Debian/Ubuntu-based:    `/Data/new_linux_install/firewall_e_aggiornamenti-Debian.sh`
Fedora-based:           `/Data/new_linux_install/firewall_e_aggiornamenti-Fedora.sh`

**Script bash n.2** (pacchetti aggiuntivi) punto 6:
Debian/Ubuntu-based:    `/Data/new_linux_install/install_packages-Debian.sh`
Fedora-based:           `/Data/new_linux_install/install_packages-Fedora.sh`