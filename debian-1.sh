#!/bin/bash
# Post installazione di Debian (1) con ambiente desktop KDE Plasma
# Questo script deve essere eseguito con i privilegi di root

if [ "$(id -u)" -ne 0 ]; then
    echo "Errore: Questo script deve essere eseguito con i privilegi di amministratore."
    echo "Usa il comando 'sudo' o passa all'utente root per eseguire questo script."
    exit 1
fi


# Aggiungere account utente personale al gruppo sudo/wheel

# Creare punti di mount per le altre partizioni
mkdir /Data && chown luca:luca /Data
mkdir /mnt/Games && chown luca:luca /mnt/Games

# Modificare /etc/fstab
# Aggiungere voci per le altre partizioni: Data, Games, etc.

### APT package manager ###
# Aggiungere repository 'contrib' e 'non-free' alle sorgenti di apt
nano /etc/apt/sources.list
# Aggiungere architettura a 32-bit (per le librerie di Steam)
dpkg --add-architecture i386; apt update
# Non considerare pacchetti raccomandati come dipendenze
echo -e "APT::Install-Recommends \"false\";" >> "/etc/apt/apt.conf.d/90custom"

### flatpak e flathub ###
apt install flatpak
apt install kde-config-flatpak plasma-discover-backend-flatpak
flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo

### Firewall e anti-virus ###
apt install clamav clamav-base clamav-freshclam ufw gufw

systemctl disable clamav-freshclam
systemctl stop clamav-freshclam
freshclam

systemctl enable ufw
/usr/sbin/ufw enable

