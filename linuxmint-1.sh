#!/bin/bash
# Post installazione di Linux Mint (1)

# Aggiorna il timestamp dell'utente senza eseguire un comando
sudo -v

# Configura e attiva il firewall ufw (già installato in Linux Mint)
echo -e "Configurazione e attivazione del firewall..."
sudo ufw limit 22/tcp
#sudo ufw allow 80/tcp
#sudo ufw allow 443/tcp
sudo ufw default deny incoming  
sudo ufw default allow outgoing
sudo ufw enable
echo -e "Ricordati di aggiungere regole per KDE Connect e Samba\n"

# Aggiorna elenco dei pacchetti disponibili
echo "Aggioramento indice dei pacchetti..."
sudo apt-get update
echo 

# Rimuove pacchetti indesiderati
echo "Rimozione pacchetti indesiderati..."
sudo apt-get purge drawing hexchat hexchat-common hypnotix onboard redshift-gtk redshift rhythmbox rhythmbox-data librhythmbox-core10 sticky
echo

# Rimuove localizzazioni superflue di Libreoffice
sudo apt-get remove libreoffice-help-de libreoffice-help-es libreoffice-help-fr libreoffice-help-pt libreoffice-help-pt-br libreoffice-help-ru libreoffice-help-zh-cn libreoffice-help-zh-tw libreoffice-l10n-de libreoffice-l10n-es libreoffice-l10n-fr libreoffice-l10n-pt libreoffice-l10n-pt-br libreoffice-l10n-ru libreoffice-l10n-zh-cn libreoffice-l10n-zh-tw
echo

# Rimuove font non necessari
echo "Rimozione font non necessari..."
sudo apt-get remove fonts-beng fonts-beng-extra fonts-deva fonts-deva-extra fonts-gargi fonts-gubbi fonts-gujr fonts-gujr-extra fonts-guru fonts-guru-extra fonts-kacst fonts-kacst-one fonts-kalapi fonts-khmeros-core fonts-lao fonts-lklug-sinhala fonts-lohit-beng-assamese fonts-lohit-beng-bengali fonts-lohit-deva fonts-lohit-gujr fonts-lohit-guru fonts-lohit-knda fonts-lohit-mlym fonts-lohit-orya fonts-lohit-taml-classical fonts-lohit-taml fonts-lohit-telu fonts-mlym fonts-nakula fonts-navilu fonts-orya-extra fonts-orya fonts-pagul fonts-sahadeva fonts-samyak-deva fonts-samyak-gujr fonts-samyak-mlym fonts-samyak-taml fonts-sarai fonts-sil-abyssinica fonts-sil-padauk fonts-smc-anjalioldlipi fonts-smc-chilanka fonts-smc-dyuthi fonts-smc-gayathri fonts-smc-karumbi fonts-smc-keraleeyam fonts-smc-manjari fonts-smc-meera fonts-smc-rachana fonts-smc-raghumalayalamsans fonts-smc-suruma fonts-smc-uroob fonts-smc fonts-taml fonts-telu-extra fonts-telu fonts-teluguvijayam fonts-thai-tlwg fonts-tibetan-machine fonts-tlwg-garuda-ttf fonts-tlwg-garuda fonts-tlwg-kinnari-ttf fonts-tlwg-kinnari fonts-tlwg-laksaman-ttf fonts-tlwg-laksaman fonts-tlwg-loma-ttf fonts-tlwg-loma fonts-tlwg-mono-ttf fonts-tlwg-mono fonts-tlwg-norasi-ttf fonts-tlwg-norasi fonts-tlwg-purisa-ttf fonts-tlwg-purisa fonts-tlwg-sawasdee-ttf fonts-tlwg-sawasdee fonts-tlwg-typewriter-ttf fonts-tlwg-typewriter fonts-tlwg-typist-ttf fonts-tlwg-typist fonts-tlwg-typo-ttf fonts-tlwg-typo fonts-tlwg-umpush-ttf fonts-tlwg-umpush fonts-tlwg-waree-ttf fonts-tlwg-waree fonts-yrsa-rasa
sudo dpkg-reconfigure fontconfig

sudo apt-get autoremove

# Aggiorna pacchetti software
sudo apt-get upgrade

# Crea punto di mount /Data, per la partizione ext4 "Data"
echo "Creazione punto di mount /Data ..."
DATA="66c842ca-ee75-402a-80af-558d65826748"
sudo mkdir /Data && sudo chown luca:luca /Data

# Crea punto di mount /mnt/ISO-IMAGES, per la partizione vfat "ISO-IMAGES"
echo "Creazione punto di mount /Data ..."
DATA="66c842ca-ee75-402a-80af-558d65826748"
sudo mkdir /Data && sudo chown luca:luca /Data

# Crea punto di mount /mnt/Fedora, per la partizione di Fedora
#echo "Creazione punto di mount /mnt/Fedora ..."
#FEDORA="..."
#sudo mkdir /mnt/Fedora && sudo chown luca:luca /mnt/Fedora
#sudo mount -v /dev/<...> /mnt/Fedora

# Crea punto di mount /mnt/Backup, per partizione "Backup" dell'HDD esterno
echo "Creazione punto di mount /mnt/Backup ..."
BACKUP="5e27c067-1722-4178-b399-8aa81e95c244"
sudo mkdir /mnt/Backup && sudo chown luca:luca /mnt/Backup



sudo -v
# Modifica file /etc/fstab
# Vedi anche: https://wiki.archlinux.org/title/Fstab
sudo cp /etc/fstab /etc/fstab.old && echo "Creato backup del file /etc/fstab in /etc/fstab.old"
echo "Applicazione modifiche a /etc/fstab ..."
echo -e "\n# Mie aggiunte"
# # Partizione swap
# UUID=57b7ef72-88a7-4bd8-aa4c-92b4628c0207  none  swap  defaults 0 0
echo -e "\n# Punto di mount /Data, per la partizione ext4 'Data' (/dev/sda3)" | sudo tee -a /etc/fstab
echo -e "UUID=$DATA  /Data  ext4  defaults,noatime  0 2" | sudo tee -a /etc/fstab
#echo -e "\n# Punto di mount /mnt/Fedora, per la partizione di Fedora" | sudo tee -a /etc/fstab
#echo -e "UUID=$FEDORA  /mnt/Fedora  ext4  defaults,noatime,nofail  0 2" | sudo tee -a /etc/fstab
echo -e "\n# Punto di mount /media/luca/Backup, per partizione 'Backup' dell'HDD esterno" | sudo tee -a /etc/fstab
echo -e "UUID=$BACKUP  /media/luca/Backup  ext4  defaults,noatime,nofail,x-systemd.device-timeout=1ms  0 0" | sudo tee -a /etc/fstab
echo "Verificare modifiche apportate a /etc/fstab"
sudo xed /etc/fstab
# Vedi ./modifiche-fstab.md
echo -e "Fatto\n"

echo -e "Fai un backup con Timeshift\nPoi riavvia"
