#!/bin/bash
# Post installazione di Fedora KDE (1)

# Aggiorna il timestamp dell'utente senza eseguire un comando
sudo -v

# Fedora include firewalld, un "firewall service daemon", già abilitato e attivo di default
# Per informazioni: https://docs.fedoraproject.org/en-US/quick-docs/firewalld/
#sudo systemctl enable firewalld

# Modifica file di configurazione di dnf
sudo cp /etc/dnf/dnf.conf /etc/dnf/dnf.conf.old && echo "Creato backup del file /etc/dnf/dnf.conf in /etc/dnf/dnf.conf.old"
echo "# Modifiche per aumentare la velocità di download" | sudo tee -a /etc/dnf/dnf.conf
echo "install_weak_deps=False" | sudo tee -a /etc/dnf/dnf.conf
echo "keepcache=True" | sudo tee -a /etc/dnf/dnf.conf
echo "max_parallel_downloads=5" | sudo tee -a /etc/dnf/dnf.conf
echo "fastestmirror=True" | sudo tee -a /etc/dnf/dnf.conf
sudo nano /etc/dnf/dnf.conf
# Vedi ./modifiche-dnf.conf.md
echo -e "Fatto\n"

sudo -v
# Aggiorna elenco dei pacchetti disponibili
echo "Aggioramento indice dei pacchetti..."
sudo dnf check-update

sudo -v
# Rimuove pacchetti indesiderati
echo "Rimozione pacchetti indesiderati..."
sudo dnf remove kmouth elisa-player konversation

sudo -v
# Rimuove font non necessari
echo "Rimozione font non necessari..."
sudo dnf remove google-noto-naskh-arabic-vf-fonts google-noto-sans-arabic-vf-fonts google-noto-sans-armenian-vf-fonts google-noto-sans-canadian-aboriginal-vf-fonts google-noto-sans-cherokee-vf-fonts google-noto-sans-ethiopic-vf-fonts google-noto-sans-georgian-vf-fonts google-noto-sans-gurmukhi-vf-fonts google-noto-sans-hebrew-vf-fonts google-noto-sans-lao-vf-fonts google-noto-sans-sinhala-vf-fonts google-noto-sans-thaana-vf-fonts aajohan-comfortaa-fonts thai-scalable-fonts-common thai-scalable-waree-fonts khmer-os-system-fonts jomolhari-fonts lohit-assamese-fonts lohit-bengali-fonts lohit-devanagari-fonts lohit-gujarati-fonts lohit-kannada-fonts lohit-marathi-fonts lohit-odia-fonts lohit-tamil-fonts lohit-telugu-fonts paktype-naskh-basic-fonts rit-meera-new-fonts sil-mingzat-fonts sil-nuosu-fonts sil-padauk-fonts vazirmatn-vf-fonts

sudo -v
# Aggiorna pacchetti software
sudo dnf updade

# Aggiungi repo flathub.org
echo "Aggiunta del repository flathub.org ..."
flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo

# systemd
sudo -v
sudo systemctl disable ModemManager.service

# Crea punto di mount /Data, per la partizione ext4 "Data"
echo "Creazione punto di mount /Data ..."
DATA="66c842ca-ee75-402a-80af-558d65826748"
sudo mkdir /Data && sudo chown luca:root /Data
#sudo mount -v /dev/sda3 /Data

# Crea punto di mount /media/LinuxMint, per la partizione ext4 di Linux Mint
echo "Creazione punto di mount /media/LinuxMint ..."
LINUXMINT="32ba40ad-3d88-4da5-85b8-4463d91f1dd2"
sudo mkdir /media/LinuxMint && sudo chown luca:root /media/LinuxMint
#sudo mount -v /dev/sda2 /media/LinuxMint

# Crea punto di mount /media/luca/Backup, per l'HDD esterno "Backup"
echo "Creazione punto di mount /media/luca/Backup ..."
BACKUP="5e27c067-1722-4178-b399-8aa81e95c244"
sudo mkdir /media/luca/Backup && sudo chown luca:root /media/luca/Backup

sudo -v
# Modifica file /etc/fstab
# Vedi anche: https://wiki.archlinux.org/title/Fstab
sudo cp /etc/fstab /etc/fstab.old && echo "Creato backup del file /etc/fstab in /etc/fstab.old"
echo "Applicazione modifiche a /etc/fstab ..."
echo -e "\n# Mie aggiunte"
# # Partizione swap
# UUID=57b7ef72-88a7-4bd8-aa4c-92b4628c0207  none  swap  defaults 0 0
echo -e "\n# Punto di mount /Data, per la partizione ext4 \"Data\"" | sudo tee -a /etc/fstab
echo -e "UUID=$DATA  /Data ext4  defaults,noatime  0 2" | sudo tee -a /etc/fstab
echo -e "\n# Punto di mount /media/LinuxMint, per la partizione ext4 di Linux Mint" | sudo tee -a /etc/fstab
echo -e "UUID=$LINUXMINT  /media/LinuxMint ext4  defaults,noatime,nofail  0 2" | sudo tee -a /etc/fstab
echo -e "\n# Punto di mount /media/luca/Backup, per partizione \"Backup\" dell'HDD esterno" | sudo tee -a /etc/fstab
echo -e "UUID=$BACKUP  /media/luca/Backup ext4  defaults,noatime,nofail,x-systemd.device-timeout=1ms  0 0" | sudo tee -a /etc/fstab
echo "Verificare modifiche apportate a /etc/fstab"
sudo nano /etc/fstab
# Vedi ./modifiche-fstab.md
echo -e "Fatto\n"

echo "Riavvia"
