#!/bin/bash
# Post installazione di Linux Mint (2)

# Collega le cartelle Documenti, Immagini, Musica, Video (presenti in Data) alle corrispettive directory in Home
rmdir $HOME/Documenti && ln -s /Data/Documenti $HOME/Documenti
rmdir $HOME/Immagini && ln -s /Data/Immagini $HOME/Immagini
rmdir $HOME/Musica && ln -s /Data/Musica $HOME/Musica
rmdir $HOME/Video && ln -s /Data/Video $HOME/Video
ln -s /Data/eseguibili/AppImage $HOME/AppImage
ln -s /Data/Git $HOME/Git
ln -s /Data/Libri $HOME/Libri
ln -s /Data/Programmazione $HOME/Programmazione

mkdir -p $HOME/Scaricati/ebook
mkdir $HOME/.icons

# Crea cartelle per config files di VSCodium
mkdir -p $HOME/.config/VSCodium/User
mkdir $HOME/.vscode-oss
ln -si /Data/.vscode-oss/extensions $HOME/.vscode-oss/extensions

# Aggiorna il timestamp dell'utente senza eseguire un comando
sudo -v

# Alcuni pacchetti software aggiuntivi
sudo apt-get update
# utilità
sudo apt-get install bleachbit clamav clamav-base clamav-freshclam exa gparted htop ibus scrcpy testdisk tldr xclip zsh
#sudo freshclam
# internet
sudo apt-get install chromium gufw nextcloud-desktop samba
# programmazione
sudo apt-get install android-sdk cmake cmake-qt-gui git gh konsole kate make python3-pip python3-venv sqlite3 stow vim
sudo apt deb /Data/eseguibili/packages/codium.deb
# multimedia
sudo apt-get install audacious brasero ffmpeg gimp inkscape pavucontrol vlc
# studio e istruzione
sudo apt install kalgebra texstudio texlive-base texlive-latex-base texstudio-doc texstudio-l10n #cantor labplot kmplot octave cantor-backend-kalgebra cantor-backend-octave cantor-backend-python3
# ufficio
sudo apt-get install gramps pdfarranger xournalpp
#sudo apt deb /Data/eseguibili/packages/zoom_amd64.deb
# temi e fonts extra
sudo apt-get install breeze-icon-theme libreoffice-style-breeze fonts-crosextra-carlito fonts-crosextra-caladea #mint-backgrounds-tessa
# virtualizzazione
sudo apt-get install qemu-kvm libvirt-daemon-system bridge-utils virt-manager
# videogiochi
sudo apt install mono-devel steam

# Strawberry music player
sudo add-apt-repository ppa:jonaski/strawberry
sudo apt-get update
sudo apt-get install strawberry

# Calibre ebook manager
sudo -v && wget -nv -O- https://download.calibre-ebook.com/linux-installer.sh | sudo sh /dev/stdin

# yt-dlp
curl -L https://github.com/yt-dlp/yt-dlp/releases/latest/download/yt-dlp -o $HOME/.local/bin/yt-dlp
chmod a+rx $HOME/.local/bin/yt-dlp
# Per aggiornare yt-dlp: sudo yt-dlp -U

# Flatpak
flatpak install flathub fr.handbrake.ghb
flatpak install flathub org.freac.freac
flatpak install flathub org.musicbrainz.Picard
flatpak install flathub org.onlyoffice.desktopeditors

## forse:
flatpak org.kde.cantor org.kde.kalgebra org.kde.kmplot org.kde.labplot2

#flatpak install flathub com.obsproject.Studio com.obsproject.Studio.Plugin.SceneSwitcher com.obsproject.Studio.Plugin.OBSVkCapture com.obsproject.Studio.Plugin.MoveTransition com.obsproject.Studio.Plugin.Gstreamer
#flatpak install flathub com.usebottles.bottles -y
#flatpak install flathub im.riot.Riot -y
#flatpak install flathub net.lutris.Lutris -y
#flatpak install flathub org.gnome.Chess -y
#flatpak install flathub org.gnome.Mahjongg -y
#flatpak install flathub org.gnome.Mines -y
#flatpak install flathub org.kde.akregator -y
#flatpak install flathub org.kde.kdenlive -y
#flatpak install flathub org.kde.kalzium -y
#flatpak install flathub org.kde.kasts -y
#flatpak install flathub org.kde.kpat -y
#flatpak install flathub org.kde.kstars -y
#flatpak install flathub org.kde.ktouch -y
#flatpak install flathub org.remmina.Remmina -y

# Git configuration
git config --global user.name "Luca Pellegrini"
git config --global user.email luca.pellegrini@disroot.org
git config --global core.editor "flatpak run re.sonny.Commit"
git config --global init.defaultBranch "main"

# Importa le chiavi SSH da /Data/.ssh in /home/luca/.ssh
cp -r /Data/.ssh $HOME/.ssh
# Aggiungi la chiave privata SSH all'ssh-agent
eval "$(ssh-agent -s)"
echo ""
ssh-add $HOME/.ssh/git

# config-files
git clone  -v https://git.disroot.org/luca-pellegrini/config-files.git $HOME/config-files
cd "$HOME/config-files" && chmod +x setup.sh && ./setup.sh

# Personalizza il menu di GRUB

# Modifica /etc/default/grub
# Vedi /Data/Git/linux-setup/modifiche-grub.md

echo -e "Fai un backup con Timeshift!\n"

# Nvidia drivers
echo -e "... e installa i driver Nvidia tramite il 'Gestore dei driver'\n"
