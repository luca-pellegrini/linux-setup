# Modifiche a /etc/dnf/dnf.conf

Modifiche da apportare al file di configurazione di **dnf** (`/etc/dnf/dnf.conf`), nello script (1) post installazione di Fedora.

Aggiungere all'inizio del file, se mancante:
```ini
# see `man dnf.conf` for defaults and possible options
```

Modificare `installonly_limit=3` in `installonly_limit=5` (aumenta il numero di vecchi pacchetti kernel mantenuti sul disco, quando si aggiorna a una nuova versione del kernel).

Aggiungere alla fine del file:
```ini
# Modifiche per aumentare la velocità di download
install_weak_deps=False
keepcache=True
max_parallel_downloads=5
fastestmirror=True
```
