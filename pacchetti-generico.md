# Pacchetti software installati/da installare - GNU/Linux generico
Elenco dei pacchetti software installati/da installati, in ambiente GUN/Linux sul mio PC.

## Amministrazione di sistema e driver
* ufw (già installato in Linux Mint) o altro firewall
* gufw (GUI per ufw; già installato in Linux Mint)
* bleachbit
* clamav
* Flatseal (flatpak)
* gparted
* htop
* nvidia-driver-525
* stow

## Utilità
* Bitwarden (AppImage/pacchetto scaricato)
* balenaEtcher (AppImage)
* Collision (flatpak), gtkhash
* GNOME Clocks (flatpak)
* GNOME Maps (flatpak)
* scrcpy
* testdisk
* xclip

## Internet
* akregator
* chromium
* Element (pacchetto scaricato/flatpak)
* firefox
* nextcloud desktop client (pacchetto nativo/AppImage)
* Telegram Desktop (eseguibile scaricato)
* thunderbird
* Tor Browser (eseguibile scaricato)
* Zoom (pacchetto scaricato)

## Ufficio
* Apostrophe (flatpak) (?)
* Calibre (eseguibile scaricato/pacchetto nativo)
* gramps
* Joplin (AppImage)
* libreoffice
* libreoffice-style-breeze
* Onlyoffice Desktop Editors (flatpak) (?)
* pdfarranger
* texstudio
* xournalpp (pacchetto nativo/flatpak)
* xreader (Cinnamon) / okular (KDE)

## Programmazione
* Commit (flatpak)
* git
* Gittyup (flatpak) (?)
* make
* meld (Cinnamon/GNOME) / kompare (KDE)
* python3-pip
* python3-venv
* sqlitebrowser
* vim
* VSCodium (pacchetto scaricato)
* xed (Cinnamon), kate/kwrite (KDE)
* zsh

## Istruzione
* kalgebra
* octave (?)
* texstudio

## Audio e video
* audacious
* audacity (?)
* brasero
* ffmpeg
* fre:ac (flatpak)
* MusicBrainz Picard (pacchetto nativo/flatpak)
* strawberry music player (pacchetto nativo/PPA)
* vlc

## Grafica
* gimp
* inkscape
* kolourpaint / drawing (?)
* krita (?)
* simple-scan

## Giochi
* Minetest (flatpak) (?)
* steam (architettura i386)
  * Cities: Skyline

Forse:
* NVIDIA CUDA (può causare problemi)

* * *

## File .deb/.rpm scaricati
* codium (VSCodium): https://github.com/VSCodium/vscodium/releases/latest/
* strawberry music player: https://www.strawberrymusicplayer.org/#download 
  * PPA (Ubuntu & derivative): `sudo add-apt-repository ppa:jonaski/strawberry`
* zoom: https://zoom.us/download#client_4meeting

## AppImage
* Bitwarden: https://vault.bitwarden.com/download/?app=desktop&platform=linux
* Joplin: https://joplinapp.org/help/#desktop-applications
* balenaEtcher: https://www.balena.io/etcher#download-etcher

## Archivi/file eseguibili scaricati
* calibre: https://calibre-ebook.com/download_linux
```bash
sudo -v && wget -nv -O- https://download.calibre-ebook.com/linux-installer.sh | sudo sh /dev/stdin
```

* Telegram: https://desktop.telegram.org
  * https://telegram.org/dl/desktop/linux
* Tor Browser: https://www.torproject.org/download/
* homebrew: https://brew.sh/
```bash
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
```

## Flatpak (flathub.org)
Importanti:
* com.github.tchx84.Flatseal
* dev.geopjr.Collision
* org.freac.freac
* org.gnome.Maps
* org.gnome.clocks
* org.musicbrainz.Picard
* org.onlyoffice.desktopeditors
* re.sonny.Commit

Forse:
* com.discordapp.Discord
* com.github.hugolabe.Wike
* com.github.Murmele.Gittyup
* com.usebottles.bottles
* fr.handbrake.ghb
* im.riot.Riot
* net.minetest.Minetest
* org.gnome.gitlab.somas.Apostrophe
* org.gnome.Mahjongg
* org.gnome.Mines
* org.openttd.OpenTTD
* org.supertuxproject.SuperTux
