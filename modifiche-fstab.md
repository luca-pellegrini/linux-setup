Modifiche da apportare al file /etc/fstab dopo un'installazione Linux

Alla riga che contiente le istruzioni per la partizione / del sistema, ad esempio come quella seguente (da un'installazione Fedora):
```
UUID=b7bd0443-3c74-489c-86c8-ce4bf8c6e3ac  /  ext4  defaults  1 1
```
dopo `defaults` aggiungere, senza spazi, `,noatime`

Aggiungere alle fine del file:
```
# Mie aggiunte: monta la partizione ext4 "Data" presente sull'SSD, al punto di mount /Data
UUID=66c842ca-ee75-402a-80af-558d65826748  /Data ext4  defaults,noatime  0 2
```
