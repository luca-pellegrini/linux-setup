#!/bin/bash
# Post installazione di Debian (2) con ambiente desktop KDE Plasma

# Collega le cartelle Documenti, Immagini, Musica, Video (presenti in Data) alle corrispettive directory in Home
rmdir $HOME/Documenti && ln -s /Data/Documenti $HOME/Documenti
rmdir $HOME/Immagini && ln -s /Data/Immagini $HOME/Immagini
rmdir $HOME/Musica && ln -s /Data/Musica $HOME/Musica
rmdir $HOME/Video && ln -s /Data/Video $HOME/Video
ln -s /Data/eseguibili/AppImage $HOME/AppImage
ln -s /Data/Git $HOME/Git
ln -s /Data/Libri $HOME/Libri
ln -s /Data/Programmazione $HOME/Programmazione

mkdir -p $HOME/.local/bin
mkdir -p $HOME/bin
mkdir -p $HOME/.local/share/fonts
mkdir -p $HOME/.icons
mkdir -p $HOME/Scaricati/ebook

# Crea cartelle per config files di VSCodium
mkdir -p $HOME/.config/VSCodium/User
mkdir $HOME/.vscode-oss
ln -si /Data/.vscode-oss/extensions $HOME/.vscode-oss/extensions

# Aggiorna il timestamp dell'utente senza eseguire un comando
sudo -v

# Alcuni pacchetti software aggiuntivi
sudo apt update
# Componenti aggiuntivi per KDE Plasma
sudo apt install kwin-addons kcharselect plasma-workspace-wallpapers
# Gestore di pacchetti Synaptic
sudo apt install synaptic
# Programmazione
sudo apt install android-sdk build-essential cmake cmake-qt-gui gcc g++ git kompare make python3-pip python3-venv sqlite3 stow vim
sudo apt install /Data/eseguibili/packages/codium.deb
# Utilità varie
sudo apt install exa gparted htop ibus info neofetch scrcpy skanpage testdisk tldr xclip zsh
# Internet
sudo apt install chromium nextcloud-desktop samba thunderbird
# Multimedia
sudo apt install audacious audacious-plugins brasero ffmpeg gimp inkscape libdvd-pkg kdenlive kolourpaint krita vlc #pavucontrol
# Studio e istruzione
sudo apt install kalgebra texstudio texlive-base texlive-latex-base texstudio-doc texstudio-l10n #cantor labplot kmplot octave cantor-backend-kalgebra cantor-backend-octave cantor-backend-python3
# Ufficio
sudo apt install pdfarranger xournalpp #gramps
# temi e fonts extra
sudo apt install fonts-crosextra-carlito fonts-crosextra-caladea
# virtualizzazione
#sudo apt install qemu-kvm libvirt-daemon-system bridge-utils virt-manager
# videogiochi
sudo apt install mono-devel steam

# Calibre ebook manager
#sudo -v && wget -nv -O- https://download.calibre-ebook.com/linux-installer.sh | sudo sh /dev/stdin

# yt-dlp
curl -L https://github.com/yt-dlp/yt-dlp/releases/latest/download/yt-dlp -o $HOME/.local/bin/yt-dlp
chmod +rx $HOME/.local/bin/yt-dlp
# Per aggiornare yt-dlp: yt-dlp -U

# Flatpak
flatpak install flathub fr.handbrake.ghb
flatpak install flathub org.freac.freac
flatpak install flathub org.musicbrainz.Picard
flatpak install flathub org.onlyoffice.desktopeditors

## forse:
#org.kde.cantor org.kde.kalgebra org.kde.kmplot org.kde.labplot2

#flatpak install flathub com.obsproject.Studio com.obsproject.Studio.Plugin.SceneSwitcher com.obsproject.Studio.Plugin.OBSVkCapture com.obsproject.Studio.Plugin.MoveTransition com.obsproject.Studio.Plugin.Gstreamer
#flatpak install flathub com.usebottles.bottles -y
#flatpak install flathub im.riot.Riot -y
#flatpak install flathub net.lutris.Lutris -y
#flatpak install flathub org.kde.kdenlive -y
#flatpak install flathub org.kde.kstars -y
#flatpak install flathub org.kde.ktouch -y
#flatpak install flathub org.remmina.Remmina -y

# Git configuration
git config --global user.name "Luca Pellegrini"
git config --global user.email luca.pellegrini@disroot.org
git config --global core.editor "flatpak run re.sonny.Commit"
git config --global init.defaultBranch "main"

# Importa le chiavi SSH da /Data/.ssh in /home/luca/.ssh
cp -r /Data/.ssh $HOME/.ssh
# Aggiungi la chiave privata SSH all'ssh-agent
eval "$(ssh-agent -s)"
echo ""
ssh-add $HOME/.ssh/id_rsa

# config-files
git clone  -v https://git.disroot.org/luca-pellegrini/config-files.git $HOME/config-files
cd "$HOME/config-files" && chmod +x setup.sh && ./setup.sh

# Personalizza il menu di GRUB

# Modifica /etc/default/grub
# Vedi /Data/Git/linux-setup/modifiche-grub.md

echo -e "Fai un backup con Timeshift!\n"

# Nvidia drivers
echo -e "... e installa i driver Nvidia tramite il 'Gestore dei driver'\n"
