# Tema personalizzato per il menu di GRUB

Basato sul repository [distro-grub-themes](https://github.com/AdisonCavani/distro-grub-themes)

## Variabili

Linux Mint
```shell
BOOT_GRUB_LOCATION="/boot/grub"
DISTRO="mint"
```

Fedora
```shell
BOOT_GRUB_LOCATION="/boot/grub2"
DISTRO="fedora"
```

## Copia file del tema e modifica /etc/default/grub

```shell
echo "Copia dei file del tema in $BOOT_GRUB_LOCATION/themes ..."
sudo mkdir $BOOT_GRUB_LOCATION/themes
cd /Data/Git/Altri/distro-grub-themes/customize
sudo cp -r $DISTRO/ $BOOT_GRUB_LOCATION/themes
echo

echo "Creazione backup di /etc/default/grub ..."
sudo cp /etc/default/grub /etc/default/grub.old
echo -e "GRUB_GFXMODE=1920x1080" | sudo tee -a /etc/default/grub
echo -e "GRUB_THEME=\"$BOOT_GRUB_LOCATION/themes/$DISTRO/theme.txt\"" | sudo tee -a /etc/default/grub
echo "Verificare modifiche apportate a /etc/default/grub"
sudo xed /etc/default/grub
```

`GRUB_TERMINAL_OUTPUT="console"` e `GRUB_DISABLE_SUBMENU=true` devono essere commentati.

## Aggiorna file di configurazione di GRUB

Linux Mint:
```shell
sudo update-grub
```

Fedora:
```shell
sudo grub2-mkconfig -o /boot/grub2/grub.cfg
sudo grub2-mkconfig -o /boot/efi/EFI/fedora/grub.cfg
```
