#!/bin/bash
# Post installazione di Fedora (2)

# Collega le cartelle Documenti, Immagini, Musica, Video, ... (presenti in Data) alle corrispettive directory in Home
rmdir $HOME/Documenti && ln -s /Data/Documenti $HOME/Documenti
rmdir $HOME/Immagini && ln -s /Data/Immagini $HOME/Immagini
rmdir $HOME/Musica && ln -s /Data/Musica $HOME/Musica
rmdir $HOME/Video && ln -s /Data/Video $HOME/Video
ln -s /Data/eseguibili/AppImage $HOME/AppImage
ln -s /Data/Git $HOME/Git
ln -s /Data/Libri $HOME/Libri
ln -s /Data/Programmazione $HOME/Programmazione

mkdir -p $HOME/.local/bin
mkdir -p $HOME/bin
mkdir -p $HOME/.icons
mkdir -p $HOME/Scaricati/ebook

# Aggiorna il timestamp dell'utente senza eseguire un comando
sudo -v

# Alcuni pacchetti software aggiuntivi
# clamav:
sudo dnf install clamav clamav-doc clamav-update
sudo freshclam
# internet, email, gparted:
sudo dnf install --setop="install_weak_deps=False" nextcloud-client nextcloud-client-dolphin falkon konqueror thunderbird gparted
# utilità varie:
sudo dnf install --setop="install_weak_deps=False" exa htop info libreoffice-help-it libreoffice-langpack-it pdfarranger testdisk xclip zsh
# programmazione:
sudo dnf install --setop="install_weak_deps=False" clang-libs cmake cmake-gui gcc git kompare make python3-idle python3-pip ShellCheck stow
sudo dnf install /Data/eseguibili/packages/codium.rpm
# multimedia:
sudo dnf install --setop="install_weak_deps=False" audacious audacious-plugins brasero gimp inkscape k3b krita picard strawberry
# studio e istruzione:
sudo dnf install --setop="install_weak_deps=False" kalgebra texstudio xournalpp #cantor LabPlot octave

# virtualizzazione (vedi anche: https://docs.fedoraproject.org/en-US/quick-docs/getting-started-with-virtualization/index.html )
#sudo dnf install @virtualization --setop="install_weak_deps=False"
#sudo systemctl start libvirtd
#sudo systemctl enable libvirtd

# scrcpy non è disponibile nei repo di Fedora
# bisognerebbe installarlo do un repository Copr

# yt-dlp
curl -L https://github.com/yt-dlp/yt-dlp/releases/latest/download/yt-dlp -o $HOME/.local/bin/yt-dlp
chmod a+rx $HOME/.local/bin/yt-dlp
# Per aggiornare yt-dlp: sudo yt-dlp -U

# Flatpak
#flatpak install flathub fr.handbrake.ghb -y
#flatpak install flathub com.obsproject.Studio com.obsproject.Studio.Plugin.SceneSwitcher com.obsproject.Studio.Plugin.OBSVkCapture com.obsproject.Studio.Plugin.MoveTransition com.obsproject.Studio.Plugin.Gstreamer
flatpak install flathub org.freac.freac -y

# Git configuration
git config --global user.name "Luca Pellegrini"
git config --global user.email luca.pellegrini@disroot.org
git config --global core.editor "kate --startanon"
git config --global core.excludesfile $HOME/.gitignore_global
git config --global init.defaultBranch "main"

# config-files Git repo
git clone -v --branch fedora-kde https://git.disroot.org/luca-pellegrini/config-files.git $HOME/config-files
cd $HOME/config-files && stow desktop-files rsync varie yt-dlp zsh

# Crea cartelle per config files di VSCodium
mkdir -p $HOME/.config/VSCodium/User
mkdir $HOME/.vscode-oss
ln -sf /Data/.vscode-oss/extensions $HOME/.vscode-oss/extensions

# Importa le chiavi SSH da /Data/.ssh in $HOME/.ssh
cp -rp /Data/.ssh $HOME/.ssh
#sudo chown luca:luca -R $HOME/.ssh
# Aggiungi la chiave privata SSH all'ssh-agent
#ssh-add $HOME/.ssh/git

# Personalizza il menu di GRUB
# ...
