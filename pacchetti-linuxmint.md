# Pacchetti software installati/da installare - Linux Mint
Elenco dei pacchetti software installati/da installare, in Linux Mint sul mio PC.

## apt: repository Linux Mint, Ubuntu e PPA
aisleriot
akregator
audacious
bleachbit
brasero
breeze-icon-theme
bridge-utils
chromium
clamav
ffmpeg
gimp
gimp-help-it
gimp-help-en
git
gparted
gramps
htop
k3b (forse)
kalgebra
kate (forse; spesso da problemi)
kdenlive
kolourpaint
konsole
kwrite
language-pack-en-base
language-pack-en
language-pack-gnome-en-base
language-pack-gnome-en
libreoffice-help-en-gb
libreoffice-help-en-us
libreoffice-help-it
libreoffice-l10n-en-gb
libreoffice-l10n-en-za
libreoffice-l10n-it
libreoffice-style-breeze
libvirt-clients
libvirt-daemon-system
make
meld
nextcloud-desktop
nvidia-driver-525 (repo Ubuntu)
obs-studio
octave (forse)
pavucontrol
pdfarranger
python3-pip
python3-venv
qemu-kvm
scrcpy
sqlite3-tools
sqlite3
sqlitebrowser
steam:i386
stow
testdisk
texlive-base
texlive-latex-base
texstudio-doc
texstudio-l10n
texstudio
vim
virt-manager
vlc
xclip
xournalpp
zsh

element-desktop
```bash
sudo apt install -y apt-transport-https
sudo wget -O /usr/share/keyrings/element-io-archive-keyring.gpg https://packages.element.io/debian/element-io-archive-keyring.gpg
echo "deb [signed-by=/usr/share/keyrings/element-io-archive-keyring.gpg] https://packages.element.io/debian/ default main" | sudo tee /etc/apt/sources.list.d/element-io.list
sudo apt update
sudo apt install element-desktop
```

strawberry (PPA)
```bash
sudo add-apt-repository ppa:jonaski/strawberry
sudo apt update
sudo apt install strawberry
```

## File .deb scaricati
* codium (VSCodium): https://github.com/VSCodium/vscodium/releases/
* Zoom: https://zoom.us/download#client_4meeting

## AppImage
* Bitwarden: https://vault.bitwarden.com/download/?app=desktop&platform=linux
* Joplin: https://joplinapp.org/help/#desktop-applications
* balenaEtcher: https://www.balena.io/etcher#download-etcher

## Archivi/file eseguibili scaricati
* calibre: https://calibre-ebook.com/download_linux
```bash
sudo -v && wget -nv -O- https://download.calibre-ebook.com/linux-installer.sh | sudo sh /dev/stdin
```
* Telegram: https://desktop.telegram.org
    * https://telegram.org/dl/desktop/linux)
* Tor Browser: https://www.torproject.org/download/:

## Flatpak (flathub.org)
Importanti:
* com.github.tchx84.Flatseal
* dev.geopjr.Collision
* org.freac.freac
* org.gnome.Maps
* org.gnome.clocks
* org.musicbrainz.Picard
* org.onlyoffice.desktopeditors
* re.sonny.Commit

Forse:
* com.discordapp.Discord
* com.github.hugolabe.Wike
* com.github.Murmele.Gittyup
* com.usebottles.bottles
* fr.handbrake.ghb
* net.minetest.Minetest
* org.gnome.gitlab.somas.Apostrophe
* org.gnome.Mahjongg
* org.gnome.Mines
* org.openttd.OpenTTD
* org.supertuxproject.SuperTux


## Applicazioni web
* https://web.whatsapp.com      Whatsapp Web    firefox
* https://music.youtube.com     Youtube Music   firefox (+ uBlock Origin)
* https://open.spotify.com      Spotify         firefox
* https://bitwarden.devol.it    Bitwarden (devol.it)    firefox

## Sfondi
mint-backgrounds-maya
mint-backgrounds-nadia
mint-backgrounds-olivia
mint-backgrounds-petra
mint-backgrounds-qiana
mint-backgrounds-rafaela
mint-backgrounds-rebecca
mint-backgrounds-retro
mint-backgrounds-rosa
mint-backgrounds-sarah
mint-backgrounds-serena
mint-backgrounds-sonya
mint-backgrounds-sylvia
mint-backgrounds-tara
mint-backgrounds-tessa
mint-backgrounds-tina
mint-backgrounds-tricia
mint-backgrounds-ulyana
mint-backgrounds-ulyssa
mint-backgrounds-uma
mint-backgrounds-una
mint-backgrounds-vanessa
mint-backgrounds-vera
